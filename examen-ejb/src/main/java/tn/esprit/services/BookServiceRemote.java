package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Book;

@Remote
public interface BookServiceRemote {

	public void addBook(Book b);
	public void updateBook(Book b);
	public void deleteBook(Integer id);
	public Book getBookById(Integer id);
	public List<Book> getAllBook();
}
