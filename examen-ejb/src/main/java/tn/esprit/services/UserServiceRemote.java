package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.User;

@Remote
public interface UserServiceRemote {

	public void addUser(User u);
	public void updateUser(User u);
	public void deleteUser(Integer id);
	public User getUserById(Integer id);
	public List<User> getAllUser();
}
