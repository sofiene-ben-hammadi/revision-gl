package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Book
 *
 */
@Entity

public class Book implements Serializable {

	   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer idBook;
	private static final long serialVersionUID = 1L;

	public Book() {
		super();
	}   
	public Integer getIdBook() {
		return this.idBook;
	}

	public void setIdBook(Integer idBook) {
		this.idBook = idBook;
	}
	
	private String title;
	private float price;
	private String description;
	private Integer nbrOfpages;
	private boolean illustrations;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getNbrOfpages() {
		return nbrOfpages;
	}
	public void setNbrOfpages(Integer nbrOfpages) {
		this.nbrOfpages = nbrOfpages;
	}
	public boolean isIllustrations() {
		return illustrations;
	}
	public void setIllustrations(boolean illustrations) {
		this.illustrations = illustrations;
	}
	
	@ManyToOne
	private User user;

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
   
}
